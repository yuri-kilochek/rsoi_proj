#!/usr/bin/env python3

import pickle
import threading
import contextlib
from hashlib import sha256
import requests
from flask import *
import the_code as code

with open('urls.pkl', 'rb') as urls_stream:
    urls = pickle.load(urls_stream)

app = Flask(__name__)

states_lock = threading.Lock()
states = {}

@app.route('/map', methods = ['POST'])
def map_post():
    subdata = pickle.loads(request.data)
    try:
        key, value = code.map(subdata)
    except Exception as e:
        return pickle.dumps(e), 500, {'Content-Type': 'application/octet-stream'}
    key_hash = int.from_bytes(sha256(pickle.dumps(key)).digest(), 'big')
    response = requests.post(urls[key_hash % len(urls)] + '/reduce', data = pickle.dumps((key, value)),
                                                                            headers = {'Content-Type': 'application/octet-stream'})
    if response.status_code != 204:
        return response.content, response.status_code, dict(response.headers.items())
    return '', 204

@app.route('/reduce', methods = ['POST'])
def reduce_post():
    key, value = pickle.loads(request.data)
    with states_lock:
        if key in states:
            box = states[key]
            arguments = key, value, box[1]
        else:
            states[key] = box = [threading.Lock(), None]
            arguments = key, value
        box[0].acquire()
    try:
        try:
            box[1] = code.reduce(*arguments)
        except Exception as e:
            return pickle.dumps(e), 500, {'Content-Type': 'application/octet-stream'}
    finally:
        box[0].release()
    return '', 204

@app.route('/states', methods = ['GET'])
def states_get():
    result = {}
    with states_lock, contextlib.ExitStack() as stack:
        for key, box in states.items():
            stack.enter_context(box[0])
            result[key] = box[1]
    result = pickle.dumps(result)
    return result, 200, {'Content-Type': 'application/octet-stream'}

@app.route('/log', methods = ['GET'])
def log_get():
    def run():
        with open('worker.log') as log_stream:
            for line in log_stream:
                yield '<li>{}</li>'.format(escape(line))
    return ''.join(run())

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 80,
            threaded = True,
            debug = True, use_reloader = False)

