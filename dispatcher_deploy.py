#!/usr/bin/env python3

import time
import pickle
from uuid import uuid4
from io import StringIO
import yaml
import boto3
import paramiko
import requests

with open('config.yaml') as config_stream:
    config = yaml.load(config_stream)

with open('credentials.yaml') as credentials_stream:
    credentials = yaml.load(credentials_stream)

key_pair = None
instance = None
address = None
try:
    print('Authenticating...')
    session = boto3.Session(region_name = config['region'],
                            aws_access_key_id = credentials['access_key_id'],
                            aws_secret_access_key = credentials['access_key_secret'])
    ec2_client = session.client('ec2')
    ec2_resource = session.resource('ec2')
    print('Allocating elastic address...')
    address = ec2_resource.VpcAddress(ec2_client.allocate_address(Domain = 'vpc')['AllocationId'])
    print('Creating key pair...')
    key_pair = ec2_resource.create_key_pair(KeyName = str(uuid4()))
    while True:
        print('Starting up instance...')
        instance = ec2_resource.create_instances(InstanceType = config['dispatcher_instance_type'],
                                                 ImageId = config['dispatcher_image_id'],
                                                 KeyName = key_pair.name,
                                                 MinCount = 1, MaxCount = 1)[0]
        instance.wait_until_running()
        print('Binding address to instance...')
        address.associate(InstanceId = instance.id)

        print('Connecting via SSH...')
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        while True:
            try:
                ssh.connect(hostname = address.public_ip,
                            username = config['dispatcher_username'],
                            pkey = paramiko.RSAKey.from_private_key(StringIO(key_pair.key_material)))
                break
            except paramiko.ssh_exception.NoValidConnectionsError:
                print('  Retrying...')
                time.sleep(1)

        print('Configuring environment...')
        err = ssh.exec_command('''
            sudo apt-get update -y || exit 1

            sudo apt-get install libssl-dev -y || exit 1
            sudo apt-get install libffi-dev -y || exit 1

            sudo apt-get install python3-pip -y || exit 1

            sudo pip3 install PyYAML || exit 1
            sudo pip3 install boto3 || exit 1
            sudo pip3 install paramiko || exit 1
            sudo pip3 install requests || exit 1
            sudo pip3 install flask || exit 1
        ''')[2]
        if err.channel.recv_exit_status() == 0:
            break
        for line in err:
            line = line.strip()
            if line:
                print('  {}'.format(line))
        print('Configuration failed (possibly spuriously, see log above). Trying again...')
        instance.terminate()
        instance.wait_until_terminated()

    print('Uploading files...')
    sftp = ssh.open_sftp()
    sftp.mkdir('rsoi_proj')
    sftp.chdir('rsoi_proj')
    sftp.put('config.yaml', 'config.yaml')
    sftp.put('credentials.yaml', 'credentials.yaml')
    sftp.put('dispatcher.py', 'dispatcher.py')
    sftp.put('dispatcher.html', 'dispatcher.html')
    sftp.put('controller.py', 'controller.py')
    sftp.put('controller.html', 'controller.html')
    sftp.put('worker.py', 'worker.py')

    print('Starting dispatcher...')
    out = ssh.exec_command('''
        cd rsoi_proj
        nohup sudo python3 dispatcher.py < /dev/null > dispatcher.log 2>&1 &
        disown
    ''')[1]
    out.channel.recv_exit_status()

    time.sleep(3)
    try:
        requests.get('http://{}'.format(address.public_ip))
        print('http://{}'.format(address.public_ip))
    except requests.exceptions.ConnectionError:
        print('Startup failed:')
        with sftp.file('controller.log', 'r') as log:
            for line in log:
                print('  {}'.format(line))
except Exception as e:
    if instance is not None:
        if address is not None:
            address.association.delete()
            address.release()
        instance.terminate()

    raise
finally:
    if key_pair is not None:
        key_pair.delete()

