def partition(data):
    return range(int(data.read().decode()))

def map(subdata):
    return subdata % 17, subdata

def reduce(key, value, state = 0):
    return state + value

def render(states, result):
    for key, state in states.items():
        result.write('{} {}\n'.format(key, state).encode())

