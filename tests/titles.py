def partition(data):
    a, b = [int(x) for x in data.read().decode().split(' ')]
    return range(a, b)

def map(subdata):
    import requests
    from bs4 import BeautifulSoup as bs

    title = ''
    r = requests.get('https://habrahabr.ru/post/{}/'.format(subdata))
    if r.status_code == 200:
        soup = bs(r.text, 'html.parser')
        title = soup.find('title').text.split(' / ')[0]

    return title, subdata

def reduce(key, value, state = 0):
    return value

def render(states, result):
    for key, state in states.items():
        result.write('{} https://habrahabr.ru/post/{}/\n'.format(key, state).encode())
