#!/usr/bin/env python3

import time
import pickle
from uuid import uuid4
from io import StringIO
import yaml
import boto3
import paramiko
import requests
from flask import *

with open('config.yaml') as config_stream:
    config = yaml.load(config_stream)

with open('credentials.yaml') as credentials_stream:
    credentials = yaml.load(credentials_stream)

session = boto3.Session(region_name = config['region'],
                        aws_access_key_id = credentials['access_key_id'],
                        aws_secret_access_key = credentials['access_key_secret'])
sts_client = session.client('sts')

app = Flask(__name__, template_folder = './')

def stream_template(template_name, **context):
    app.update_template_context(context)
    t = app.jinja_env.get_template(template_name)
    rv = t.stream(context)
    return rv

@app.route('/', methods = ['GET'])
def index_get():
    return render_template('dispatcher.html', region = '',
                                              role_arn = '',
                                              external_id = '',
                                              instance_type = '',
                                              messages = None)

@app.route('/', methods = ['POST'])
def index_post():
    region = request.form['region']
    role_arn = request.form['role_arn']
    external_id = request.form['external_id']
    instance_type = request.form['instance_type']

    def run():
        key_pair = None
        instance = None
        address = None
        try:
            yield '<li>Authenticating...</li>'
            credentials = sts_client.assume_role(RoleArn = role_arn,
                                                 ExternalId = external_id,
                                                 RoleSessionName = str(uuid4()))['Credentials']
            session_data = {
                'region_name': region,
                'aws_access_key_id': credentials['AccessKeyId'],
                'aws_secret_access_key': credentials['SecretAccessKey'],
                'aws_session_token': credentials['SessionToken'],
            }
            session = boto3.Session(**session_data)
            ec2_client = session.client('ec2')
            ec2_resource = session.resource('ec2')
            yield '<li>Allocating elastic address...</li>'
            address = ec2_resource.VpcAddress(ec2_client.allocate_address(Domain = 'vpc')['AllocationId'])
            yield '<li>Creating key pair...</li>'
            key_pair = ec2_resource.create_key_pair(KeyName = str(uuid4()))
            while True:
                yield '<li>Starting up instance...</li>'
                instance = ec2_resource.create_instances(InstanceType = instance_type,
                                                         ImageId = config['controller_image_id'],
                                                         KeyName = key_pair.name,
                                                         MinCount = 1, MaxCount = 1)[0]
                instance.wait_until_running()
                yield '<li>Binding address to instance...</li>'
                address.associate(InstanceId = instance.id)

                yield '<li>Connecting via SSH...<ul>'
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                while True:
                    try:
                        ssh.connect(hostname = address.public_ip,
                                    username = config['controller_username'],
                                    pkey = paramiko.RSAKey.from_private_key(StringIO(key_pair.key_material)))
                        break
                    except paramiko.ssh_exception.NoValidConnectionsError:
                        yield '<li>Retrying...</li>'
                        time.sleep(1)
                yield '</ul></li>'

                yield '<li>Configuring environment...'
                err = ssh.exec_command('''
                    sudo apt-get update -y || exit 1

                    sudo apt-get install libssl-dev -y || exit 1
                    sudo apt-get install libffi-dev -y || exit 1

                    sudo apt-get install python3-pip -y || exit 1

                    sudo pip3 install PyYAML || exit 1
                    sudo pip3 install boto3 || exit 1
                    sudo pip3 install paramiko || exit 1
                    sudo pip3 install requests || exit 1
                    sudo pip3 install flask || exit 1
                ''')[2]
                if err.channel.recv_exit_status() == 0:
                    yield '</li>'
                    break
                yield '<ul>'
                for line in err:
                    line = line.strip()
                    if line:
                        yield '<li>{}</li>'.format(escape(line))
                yield '</ul></li>'
                yield '<li>Configuration failed (possibly spuriously, see log above). Trying again...</li>'
                instance.terminate()
                instance.wait_until_terminated()

            yield '<li>Uploading files...</li>'
            sftp = ssh.open_sftp()
            sftp.mkdir('rsoi_proj')
            sftp.chdir('rsoi_proj')
            sftp.put('config.yaml', 'config.yaml')
            sftp.put('controller.py', 'controller.py')
            sftp.put('controller.html', 'controller.html')
            with sftp.file('session.pkl', 'wb') as session_stream:
                pickle.dump(session_data, session_stream)
            with sftp.file('instance_id.pkl', 'wb') as instance_id_stream:
                pickle.dump(instance.id, instance_id_stream)
            sftp.put('worker.py', 'worker.py')

            yield '<li>Starting controller...</li>'
            out = ssh.exec_command('''
                cd rsoi_proj
                nohup sudo python3 controller.py < /dev/null > controller.log 2>&1 &
                disown
            ''')[1]
            out.channel.recv_exit_status()

            time.sleep(3)
            try:
                requests.get('http://{}'.format(address.public_ip))
                yield '''
                    <script>
                        window.location.replace('http://{}');
                    </script>
                '''.format(address.public_ip)
            except requests.exceptions.ConnectionError:
                yield '<li>Startup failed:<ul>'
                with sftp.file('controller.log', 'r') as log:
                    for line in log:
                        yield '<li>{}</li>'.format(escape(line))
                yield '</ul><li>'
        except Exception as e:
            if instance is not None:
                if address is not None:
                    address.association.delete()
                    address.release()
                instance.terminate()

            yield '<li class="error">Error: {}</li>'.format(escape(e))

            raise
        finally:
            if key_pair is not None:
                key_pair.delete()

    return Response(stream_template('dispatcher.html', region = region,
                                                       role_arn = role_arn,
                                                       external_id = external_id,
                                                       instance_type = instance_type,
                                                       messages = stream_with_context(map(Markup, run()))))

@app.route('/log', methods = ['GET'])
def log_get():
    def run():
        with open('dispatcher.log') as log_stream:
            for line in log_stream:
                yield '<li>{}</li>'.format(escape(line))
    return ''.join(run())

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 80,
            threaded = True,
            debug = True, use_reloader = False)

