#!/usr/bin/env python3

import time
import fcntl
import pickle
import random
from uuid import uuid4
from io import StringIO
import threading
import subprocess
import yaml
import boto3
import paramiko
import requests
from flask import *

with open('config.yaml') as config:
    config = yaml.load(config)

with open('session.pkl', 'rb') as session:
    session = pickle.load(session)
session = boto3.Session(**session)
ec2_resource = session.resource('ec2')

with open('instance_id.pkl', 'rb') as self_instance_id_stream:
    self_instance_id = pickle.load(self_instance_id_stream)
self_instance = ec2_resource.Instance(self_instance_id)

state_lock = threading.Lock()
state = 'initial'

class AbortException(Exception):
    pass

def check_abort():
    with state_lock:
        if state == 'aborting':
            raise AbortException()

log = []

def compute(instance_type, count):
    global state, log

    key_pair = None
    instances = None
    try:
        log += ['<li>Generating key pair...</li>']
        key_pair = ec2_resource.create_key_pair(KeyName = str(uuid4()))
        check_abort()

        def connect_ssh(instance):
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            while True:
                check_abort()
                try:
                    ssh.connect(hostname = instance.private_ip_address,
                                username = config['worker_username'],
                                pkey = paramiko.RSAKey.from_private_key(StringIO(key_pair.key_material)))
                    break
                except paramiko.ssh_exception.NoValidConnectionsError:
                    time.sleep(1)

            return ssh

        def spawn_instance():
            while True:
                check_abort()
                instance = ec2_resource.create_instances(InstanceType = instance_type,
                                                        ImageId = config['worker_image_id'],
                                                        KeyName = key_pair.name,
                                                        MinCount = 1, MaxCount = 1)[0]
                try:
                    check_abort()
                    instance.wait_until_running()
                    ssh = connect_ssh(instance)
                    check_abort()
                    out = ssh.exec_command('''
                        sudo apt-get update -y || exit 1
                        sudo apt-get install python3-pip -y || exit 1
                    ''')[1]
                    if out.channel.recv_exit_status() == 0:
                        return instance, ssh
                except AbortException:
                    instance.terminate()
                    raise
                instance.terminate()
                check_abort()
                instance.wait_until_terminated()

        log += ['']
        instances = []
        for i in range(count):
            log[-1] = '<li>Spawning instance {}/{}...</li>'.format(i + 1, count)
            instance = spawn_instance()
            instances.append(instance)
        check_abort()

        urls = []
        for instance, _ in instances:
            url = 'http://' + instance.private_ip_address
            urls.append(url)
        check_abort()

        log += ['']
        for i, (instance, ssh) in enumerate(instances):
            log[-1] = '<li>Starting instance {}/{}...</li>'.format(i + 1, count)
            sftp = ssh.open_sftp()
            check_abort()
            sftp.mkdir('rsoi_proj')
            sftp.chdir('rsoi_proj')
            check_abort()
            sftp.put('requirements.txt', 'requirements.txt')
            check_abort()
            sftp.put('the_code.py', 'the_code.py')
            check_abort()
            with sftp.file('urls.pkl', 'wb') as urls_stream:
                pickle.dump(urls, urls_stream)
            check_abort()
            sftp.put('worker.py', 'worker.py')
            check_abort()
            sftp.close()

            out = ssh.exec_command('''
                sudo pip3 install requests || exit 1
                sudo pip3 install flask || exit 1

                cd rsoi_proj || exit 1
                if [ -s requirements.txt ]; then
                    sudo pip3 install -r requirements.txt
                fi
            ''')[1]
            if out.channel.recv_exit_status() != 0:
                raise RuntimeError('Package installation failed')
            check_abort()

            out = ssh.exec_command('''
                cd rsoi_proj
                nohup sudo python3 worker.py < /dev/null > worker.log 2>&1 &
                disown
            ''')[1]
            out.channel.recv_exit_status()
            check_abort()

        time.sleep(3)

        for i, (instance, ssh) in enumerate(instances):
            check_abort()
            try:
                requests.get('http://{}/log'.format(instance.private_ip_address))
            except requests.exceptions.ConnectionError:
                sftp = ssh.open_sftp()
                sftp.chdir('rsoi_proj')
                log += ['<li>Startup failed:<ul>']
                with sftp.file('worker.log', 'r') as log_stream:
                    for line in log_stream:
                        log += ['<li>{}</li>'.format(escape(line))]
                log += ['</ul><li>']
                sftp.close()
                log += ['']
            check_abort()

            ssh.close()

        status = subprocess.call('''
            if [ -s requirements.txt ]; then
                sudo pip3 install -r requirements.txt
            fi
        ''', shell = True)
        if status != 0:
            raise RuntimeError('Package installation failed')

        import the_code as code

        with state_lock:
            state = 'running'
        with open('data', 'rb') as data_stream:
            check_abort()
            log += ['']
            for i, subdata in enumerate(code.partition(data_stream)):
                log[-1] = '<li>Processing {}...</li>'.format(i + 1)
                check_abort()
                response = requests.post(random.choice(urls) + '/map', data = pickle.dumps(subdata),
                                                                       headers = {'Content-Type': 'application/octet-stream'})
                check_abort()
                if response.status_code != 204:
                    if response.headers['Content-Type'] == 'application/octet-stream':
                        raise pickle.loads(response.content)
                    raise RuntimeError(response.status_code)

        log += ['']
        states = {}
        for i, url in enumerate(urls):
            log[-1] = '<li>Merging state {}/{}...</li>'.format(i + 1, count)
            check_abort()
            response = requests.get(url + '/states')
            check_abort()
            if response.status_code != 200:
                if response.headers['Content-Type'] == 'application/octet-stream':
                    raise pickle.loads(response.content)
                raise RuntimeError(response.status_code)
            states.update(pickle.loads(response.content))

        log += ['<li>Rendering result...</li>']
        check_abort()
        with open('result', 'wb') as result_stream:
            code.render(states, result_stream)

        log += ['<li>Finished. <a href="/result">Get result.</a></li>']
        with state_lock:
            state = 'finished'
    except AbortException:
        log += ['<li>Aborted.</li>']
        with state_lock:
            state = 'aborted'
    except Exception as e:
        log += ['<li>Error: {}</li>'.format(e)]
        with state_lock:
            state = 'aborted'
        raise
    finally:
        if key_pair is not None:
            key_pair.delete()

        if instances is not None:
            for instance, ssh in instances:
                instance.terminate()
            for instance, ssh in instances:
                instance.wait_until_terminated()

app = Flask(__name__, template_folder = './')

@app.route('/', methods = ['GET'])
def index_get():
    with state_lock:
        if state == 'terminating':
            exit()

        return render_template('controller.html', state = state,
                                                  log = list(map(Markup, log)))

@app.route('/start', methods = ['POST'])
def start_post():
    global state

    with state_lock:
        if state == 'initial':
            with open('requirements.txt', 'wb') as requirements_stream:
                requirements_stream.write(request.files['requirements'].read().decode().strip().encode())
            with open('the_code.py', 'wb') as code_stream:
                code_stream.write(request.files['code'].read())
            with open('data', 'wb') as data_stream:
                data_stream.write(request.files['data'].read())

            threading.Thread(target = compute, args = [
                request.form['instance_type'],
                int(request.form['count']),
            ]).start()

            state = 'starting'
            
    return redirect('/')

@app.route('/abort', methods = ['POST'])
def abort_post():
    global state, log

    with state_lock:
        if state in {'starting', 'running'}:
            log += ['<li>Aborting...</li>']
            state = 'aborting'

    return redirect('/')

@app.route('/terminate', methods = ['POST'])
def teminate_post():
    global state, log

    with state_lock:
        if state in {'initial', 'finished', 'aborted'}:
            log += ['<li>Terminating...</li>']
            for address in self_instance.vpc_addresses.all():
                address.association.delete()
                address.release()
            self_instance.terminate()
            state = 'terminating'

    return redirect('/')

@app.route('/result', methods = ['GET'])
def result_get():
    with state_lock:
        if state == 'finished':
            with open('result', 'rb') as result_stream:
                return result_stream.read(), 200, {
                    'Content-Type': 'application/octet-stream',
                    'Content-Disposition': 'attachment; filename=result',
                }

    return b'', 404, {
        'Content-Type': 'application/octet-stream',
    }

@app.route('/state', methods = ['GET'])
def state_get():
    with state_lock:
        return state

@app.route('/log', methods = ['GET'])
def log_get():
    def run():
        with open('controller.log') as log_stream:
            for line in log_stream:
                yield '<li>{}</li>'.format(escape(line))
    return ''.join(run())

@app.route('/log/<worker_address>', methods = ['GET'])
def worker_log_get(worker_address):
    response = requests.get('http://{}/log'.format(worker_address))
    return response.content, response.status_code, dict(response.headers.items())

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 80,
            threaded = True,
            debug = True, use_reloader = False)

